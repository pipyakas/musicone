package com.android.music.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.music.R;
import com.android.music.SongViewModel;
import com.android.music.fragments.BaseSongListFragment;

import java.util.List;

public class TrackListAdapter extends RecyclerView.Adapter<TrackListAdapter.ViewHolder> {

    private List<SongViewModel> songViewModelList;
    private static final String TAG = "TrackListAdapter";
    private RecyclerItemClickListener mListener;
    private BaseSongListFragment mFragment = null;
    private Context context;
    private int selectedPosition;

    public TrackListAdapter(Context context, List<SongViewModel> songViewModelList, RecyclerItemClickListener mListener) {
        super();
        this.context = context;
        this.songViewModelList = songViewModelList;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_item,
                viewGroup, false);
        Log.d(TAG, "onCreateViewHolder: ");
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SongViewModel songViewModel = songViewModelList.get(position);
        if (songViewModel != null) {
            if (selectedPosition == position) {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
            } else {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            }
            holder.tv_title.setText(songViewModel.getTitle());
            holder.tv_number.setText(String.valueOf(holder.getAdapterPosition() + 1));
            holder.tv_duration.setText(convertDuration(songViewModel.getDuration()));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onClickListener(songViewModel, holder.getAdapterPosition());
                }
            });
        }
        Log.d(TAG, "onBindViewHolder: ");
    }

    @Override
    public int getItemCount() {
        return songViewModelList.size();
    }

    public int getSelectedPosition() {
        Log.d(TAG, "getSelectedPosition: ");
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        Log.d(TAG, "setSelectedPosition: " + selectedPosition);
    }

    public interface RecyclerItemClickListener {
        void onClickListener(SongViewModel songViewModel, int position);
    }

    @SuppressLint("DefaultLocale")
    private static String convertDuration(int duration) {
        int minutes = (duration / 1000) / 60;
        int seconds = (duration / 1000) % 60;

        return String.format("%d:%02d", minutes, seconds);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_number, tv_duration;

        ViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.song_title);
            tv_number = itemView.findViewById(R.id.item_number);
            tv_duration = itemView.findViewById(R.id.duration);
        }
    }
}