package com.android.music.adapters;

import android.database.Cursor;
import android.util.Log;

import androidx.recyclerview.widget.RecyclerView;

import static android.content.ContentValues.TAG;

public abstract class BaseCursorAdapter<V extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<V> {
    private Cursor mCursor;
    private boolean mDataValid;
    private int mRowIDColumn;

    public abstract void onBindViewHolder(V holder, Cursor cursor);

    BaseCursorAdapter(Cursor cursor) {
        setHasStableIds(true);
        swapCursor(cursor);
    }


    @Override
    public void onBindViewHolder(V holder, int position) {
        Log.d(TAG, "BaseCursorAdapter onBindViewHolder: " + position);
        if (!mDataValid) {
            throw new IllegalStateException("Cannot bind view holder when cursor is in invalid state.");
        }
        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("Could not move cursor to position "
                    + position + " when trying to bind view holder");
        }
        onBindViewHolder(holder, mCursor);
    }


    @Override
    public int getItemCount() {
        Log.d(TAG, "BaseCursorAdapter getItemCount: " + mCursor.getCount());
        if (mDataValid) {
            return mCursor.getCount();
        } else {
            return 0;
        }
    }

    @Override
    public long getItemId(int position) {
        int idColumn = mCursor.getColumnIndex
                (android.provider.MediaStore.Audio.Media._ID);
        if (!mDataValid) {
            throw new IllegalStateException("Cannot lookup item id when cursor is in invalid state.");
        }
        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("Could not move cursor to position "
                    + position + " when trying to get an item id");
        }
        Log.d(TAG, "BaseCursorAdapter getItemId: " + mCursor.getLong(idColumn));
        return mCursor.getLong(idColumn);
    }

    public Cursor getItem(int position) {
        if (!mDataValid) {
            throw new IllegalStateException("Cannot lookup item id when cursor is in invalid state.");
        }
        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("Could not move cursor to position "
                    + position + " when trying to get an item id");
        }
        Log.d(TAG, "BaseCursorAdapter getItem: " + position);
        return mCursor;
    }

    public void swapCursor(Cursor newCursor) {
        if (newCursor == mCursor) {
            return;
        }
        if (newCursor != null) {
            mCursor = newCursor;
            mDataValid = true;
            // notify the observers about the new cursor
            notifyDataSetChanged();
        } else {
            notifyItemRangeRemoved(0, getItemCount());
            mCursor = null;
            mRowIDColumn = -1;
            mDataValid = false;
        }
        Log.d(TAG, "BaseCursorAdapter swapCursor: " + newCursor);
    }
}