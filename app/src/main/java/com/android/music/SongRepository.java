package com.android.music;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class holds the implementation code for the methods that interact with the database.
 * Using a repository allows us to group the implementation methods together,
 * and allows the WordViewModel to be a clean interface between the rest of the app
 * and the database.
 * <p>
 * For insert, update and delete, and longer-running queries,
 * you must run the database interaction methods in the background.
 * <p>
 * Typically, all you need to do to implement a database method
 * is to call it on the data access object (DAO), in the background if applicable.
 */

class SongRepository {
    static List<SongViewModel> getSongList(Context context) throws FileNotFoundException {
        final List<SongViewModel> SongList = new ArrayList<>();
        //retrieve song info
        ContentResolver musicResolver = context.getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";
        String sortOrder = MediaStore.Audio.Media.TITLE + " ASC";
        Cursor musicCursor = musicResolver.query(musicUri, null, selection, null, sortOrder);
        //iterate over results if valid
        if (musicCursor != null && musicCursor.moveToFirst()) {
            //get columns
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int albumColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ALBUM);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int albumIdColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ALBUM_ID);
            int pathColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            int durationColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            //add songs to list
            do {
                long id = musicCursor.getLong(idColumn);
                String path = musicCursor.getString(pathColumn);
                String title = musicCursor.getString(titleColumn);
                String album = musicCursor.getString(albumColumn);
                String artist = musicCursor.getString(artistColumn);
                int duration = musicCursor.getInt(durationColumn);
                long albumId = musicCursor.getLong(albumIdColumn);
                Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
                String albumArtUri = ContentUris.withAppendedId(sArtworkUri, albumId).toString();
                Bitmap albumBitmap = BitmapFactory.decodeFileDescriptor(
                        new ParcelFileDescriptor(
                                context.getContentResolver()
                                        .openFileDescriptor(ContentUris.withAppendedId(sArtworkUri, albumId), "r"))
                                .getFileDescriptor());
                SongList.add(
                        new SongViewModel(id, path, title, album, albumArtUri, albumBitmap, artist, duration));
            }
            while (musicCursor.moveToNext());
            musicCursor.close();
        }
        return SongList;
    }
}