package com.android.music;

import android.graphics.Bitmap;

import java.io.Serializable;

public class SongViewModel implements Serializable {

    private long id;
    private String path;
    private String title;
    private String album;
    private String albumArt;
    private Bitmap albumBitmap;
    private String artist;
    private int duration;

    public SongViewModel(long id, String path, String title, String album, String albumArt, Bitmap albumBitmap, String artist, int duration) {
        this.id = id;
        this.path = path;
        this.title = title;
        this.album = album;
        this.albumArt = albumArt;
        this.albumBitmap = albumBitmap;
        this.artist = artist;
        this.duration = duration;
    }

    public String getPath() {
        return path;
    }

    void setPath(String mPath) {
        this.path = mPath;
    }

    public String getTitle() {
        return title;
    }

    void setTitle(String mTitle) {
        this.title = mTitle;
    }

    public String getAlbum() {
        return album;
    }

    void setAlbum(String mAlbum) {
        this.album = mAlbum;
    }

    public String getAlbumArt() {
        return albumArt;
    }

    void setAlbumArt(String mAlbumArt) {
        this.albumArt = mAlbumArt;
    }

    public String getArtist() {
        return artist;
    }

    void setArtist(String mArtist) {
        this.artist = mArtist;
    }

    public int getDuration() {
        return duration;
    }

    void setDuration(int duration) {
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Bitmap getAlbumBitmap() {
        return albumBitmap;
    }

    public void setAlbumBitmap(Bitmap albumBitmap) {
        this.albumBitmap = albumBitmap;
    }
}