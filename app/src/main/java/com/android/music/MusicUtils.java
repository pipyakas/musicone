package com.android.music;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.format.Time;
import android.util.Log;

import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;

public class MusicUtils {

    private static final String TAG = "MusicUtils";
    private final static long[] sEmptyList = new long[0];
    private static final Object[] sTimeArgs = new Object[5];
    private static final BitmapFactory.Options sBitmapOptionsCache = new BitmapFactory.Options();
    private static final BitmapFactory.Options sBitmapOptions = new BitmapFactory.Options();
    private static final Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
    private static final HashMap<Long, Drawable> sArtCache = new HashMap<Long, Drawable>();
    // get album art for specified file
    private static final String sExternalMediaUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI.toString();
    public static IMediaPlaybackService sService = null;
    private static int sActiveTabIndex = -1;
    private static HashMap<Context, ServiceBinder> sConnectionMap = new HashMap<Context, ServiceBinder>();
    private static ContentValues[] sContentValuesCache = null;
    private static String mLastSdStatus;
    /*  Try to use String.format() as little as possible, because it creates a
     *  new Formatter every time you call it, which is very inefficient.
     *  Reusing an existing Formatter more than tripled the speed of
     *  makeTimeString().
     *  This Formatter/StringBuilder are also used by makeAlbumSongsLabel()
     */
    private static StringBuilder sFormatBuilder = new StringBuilder();
    private static Formatter sFormatter = new Formatter(sFormatBuilder, Locale.getDefault());
    private static int sArtId = -2;
    private static int sArtCacheId = -1;
    private static BitmapFactory.Options options = new BitmapFactory.Options();
    private static int sLogPtr = 0;
    private static Time sTime = new Time();

    @SuppressLint("DefaultLocale")
    public static String convertDuration(int duration) {
        int minutes = (duration / 1000) / 60;
        int seconds = (duration / 1000) % 60;

        return String.format("%d:%02d", minutes, seconds);
    }

    public static ServiceToken bindToService(Activity context) {
        return bindToService(context, null);
    }

    public static ServiceToken bindToService(Activity context, ServiceConnection callback) {
        Activity contextParent = context.getParent();
        if (contextParent == null) {
            contextParent = context;
        }
        ContextWrapper contextWrapper = new ContextWrapper(contextParent);
        contextWrapper.startService(new Intent(contextWrapper, MediaPlaybackService.class));
        ServiceBinder serviceBinder = new ServiceBinder(callback);
        if (contextWrapper.bindService((new Intent()).setClass(contextWrapper, MediaPlaybackService.class), serviceBinder, 0)) {
            sConnectionMap.put(contextWrapper, serviceBinder);
            return new ServiceToken(contextWrapper);
        }
        Log.e("Music", "Failed to bind to service");
        return null;
    }

    public static void unbindFromService(ServiceToken token) {
        if (token == null) {
            Log.e("MusicUtils", "Trying to unbind with null token");
            return;
        }
        ContextWrapper contextWrapper = token.mWrappedContext;
        ServiceBinder serviceBinder = sConnectionMap.remove(contextWrapper);
        if (serviceBinder == null) {
            Log.e("MusicUtils", "Trying to unbind for unknown Context");
            return;
        }
        contextWrapper.unbindService(serviceBinder);
        if (sConnectionMap.isEmpty()) {
            // presumably there is nobody interested in the service at this point,
            // so don't hang on to the ServiceConnection
            sService = null;
        }
    }

    public static Cursor query(Context context, Uri uri, String[] projection,
                               String selection, String[] selectionArgs, String sortOrder) {
        return query(context, uri, projection, selection, selectionArgs, sortOrder);
    }

    public static class ServiceToken {
        ContextWrapper mWrappedContext;

        ServiceToken(ContextWrapper context) {
            mWrappedContext = context;
        }
    }

    private static class ServiceBinder implements ServiceConnection {
        ServiceConnection mCallback;

        ServiceBinder(ServiceConnection callback) {
            mCallback = callback;
        }

        public void onServiceConnected(ComponentName className, android.os.IBinder service) {
            if (mCallback != null) {
                mCallback.onServiceConnected(className, service);
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (mCallback != null) {
                mCallback.onServiceDisconnected(className);
            }
        }
    }
}