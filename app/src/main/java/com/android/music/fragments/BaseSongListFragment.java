package com.android.music.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.music.ActivityMusic;
import com.android.music.MusicUtils;
import com.android.music.R;
import com.android.music.SongViewModel;
import com.android.music.adapters.TrackListAdapter;

import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the RecyclerItemClickListener
 * interface.
 */
public class BaseSongListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private Cursor mCursor;
    private RecyclerView mRecyclerView;
    // This is the Adapter being used to display the list's data.
    private TrackListAdapter mAdapter;
    private BaseSongListFragmentClickListener mListener;
    private MusicUtils.ServiceToken mToken;
    private ActivityMusic mParentActivity;
    private List<SongViewModel> songViewModelList;
    private static final String TAG = "BaseSongListFragment";

    private View mView;
    private ImageView mIcon;
    private TextView mTitle;
    private TextView mArtist;
    private ImageButton mPlayButton;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BaseSongListFragment() {

    }

    public BaseSongListFragment(List<SongViewModel> songViewModelList) {
        this.songViewModelList = songViewModelList;
    }

    public interface BaseSongListFragmentClickListener {
        void onBaseSongListFragmentClick(int position);
    }

    // TODO: Customize parameter initialization

    @SuppressWarnings("unused")
//    public static BaseSongListFragment newInstance() {
//        return new BaseSongListFragment();
//    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mParentActivity = (ActivityMusic) getActivity();
        Log.d(TAG, "onActivityCreated: ");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.all_songs_fragment, container, false);
        mAdapter = new TrackListAdapter(getContext(), songViewModelList, new TrackListAdapter.RecyclerItemClickListener() {
            @Override
            public void onClickListener(SongViewModel songViewModel, int position) {
                mListener.onBaseSongListFragmentClick(position);
            }
        });

        initializeViews();
        mRecyclerView = mView.findViewById(R.id.fragment_item_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        mPlayButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (mParentActivity.mService.isPlaying()) {
                        mParentActivity.mService.pause();
                    } else if (!mParentActivity.mService.isPlaying()) {
                        mParentActivity.mService.play();
                    }
                    mPlayButton.setImageResource(
                            mParentActivity.mService.isPlaying() ? R.drawable.pause : R.drawable.play);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });

        Log.d(TAG, "onCreateView: ");
        return mView;
    }

    private void initializeViews() {
        mIcon = mView.findViewById(R.id.header_album_art);
        mTitle = mView.findViewById(R.id.header_title);
        mArtist = mView.findViewById(R.id.header_artist);
        mPlayButton = mView.findViewById(R.id.header_play);
        mArtist.setSelected(true);
    }

    public void updateHeader() {
        try {
            mTitle.setText(mParentActivity.mService.getTitle());
            mArtist.setText(mParentActivity.mService.getArtist());
            mIcon.setImageBitmap(mParentActivity.mService.getBitmap());
            mPlayButton.setImageResource(mParentActivity.mService.isPlaying() ? R.drawable.pause : R.drawable.play);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void changeSelectedSong(int index) {
        mAdapter.notifyItemChanged(mAdapter.getSelectedPosition());
        mAdapter.setSelectedPosition(index);
        mAdapter.notifyItemChanged(index);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseSongListFragmentClickListener) {
            mListener = (BaseSongListFragmentClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement BaseSongListFragmentClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}