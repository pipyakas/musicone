package com.android.music;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Random;

/**
 * Provides "background" audio playback capabilities, allowing the
 * user to switch between activities without stopping playback.
 */
public class MediaPlaybackService extends Service {

    public static final int NOW = 1;
    public static final int NEXT = 2;
    public static final int LAST = 3;
    public static final int PLAYBACKSERVICE_STATUS = 1;

    public static final int SHUFFLE_NONE = 0;
    public static final int SHUFFLE_NORMAL = 1;

    public static final int REPEAT_NONE = 0;
    public static final int REPEAT_CURRENT = 1;
    public static final int REPEAT_ALL = 2;

    public static final String META_CHANGED = "com.android.music.metachanged";
    public static final String PLAYSTATE_CHANGED = "com.android.music.playstatechanged";

    public static final String TOGGLEPAUSE_ACTION = "com.android.music.musicservicecommand.togglepause";
    public static final String PAUSE_ACTION = "com.android.music.musicservicecommand.pause";
    public static final String PREVIOUS_ACTION = "com.android.music.musicservicecommand.previous";
    public static final String NEXT_ACTION = "com.android.music.musicservicecommand.next";
    public static final String STOP_ACTION = "com.android.music.musicservicecommand.stop";
    public static final String EXIT_ACTION = "com.android.music.musicservicecommand.exit";

    private static final String CHANNEL_ONE_ID = "MediaPlaybackService";
    private static final String CHANNEL_ONE_NAME = "com.android.music";

    private static final String TAG = "MediaPlaybackService";
    private static MediaPlayer mMediaPlayer;

    private int mShuffleMode = SHUFFLE_NONE;
    private int mRepeatMode = REPEAT_NONE;

    private List<SongViewModel> mSongList;
    private int mServiceStartId = -1;
    private int mPlayPosition = -1;
    private int mNextPlayPosition = -1;
    private boolean mServiceInUse = false;
    private boolean mIsSupposedToBePlaying = false;
    private Observer mObserver;
    private IBinder mBinder;

    @SuppressLint("HandlerLeak")
    private Handler mDelayedStopHandler = new Handler() {
        @Override
        public void handleMessage(Message message) {
            // Check again to make sure nothing is playing right now
            if (isPlaying() || mServiceInUse) {
                return;
            }
            // save the queue again, because it might have changed
            // since the user exited the music app (because of
            // party-shuffle or because the play-position changed)
            stopSelf(mServiceStartId);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        mServiceInUse = true;
        if (mBinder == null)
            mBinder = new ServiceStub(this);
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        mServiceInUse = true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel musicServiceChannel = new NotificationChannel(
                    CHANNEL_ONE_ID,
                    "AllSongsProvider Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            musicServiceChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(musicServiceChannel);
            }
        }
    }

    @Override
    public boolean onUnbind(Intent i) {
        mServiceInUse = false;
        if (isPlaying()) {
            // something is currently playing, or will be playing once
            // an in-progress action requesting audio focus ends, so don't stop the service now.
            return true;
        }
        // No active playlist, OK to stop the service right now
        stopSelf(mServiceStartId);
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mServiceStartId = startId;
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Check that we're not being destroyed while something is still playing.
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            Log.e(TAG, "Service being destroyed while still playing.");
            mMediaPlayer.release();
        }
    }

    class ServiceStub extends IMediaPlaybackService.Stub {
        WeakReference<MediaPlaybackService> mService;

        ServiceStub(MediaPlaybackService service) {
            mService = new WeakReference<>(service);
        }

        public void play() throws RemoteException {
            if (mMediaPlayer == null)
                mMediaPlayer = MediaPlayer.create(mService.get(), Uri.parse(mSongList.get(mPlayPosition).getPath()));
            else {
                try {
                    mMediaPlayer.reset();
                    mMediaPlayer.setDataSource(mSongList.get(mPlayPosition).getPath());
                    mMediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            mMediaPlayer.start();
            updateNotification();
            mIsSupposedToBePlaying = true;
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (mRepeatMode == 1)
                        mMediaPlayer.start();
                    else {
                        try {
                            next();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

        public void openFile(String path) throws RemoteException {
            if (mMediaPlayer == null)
                mMediaPlayer = MediaPlayer.create(mService.get(), Uri.parse(path));
            else {
                try {
                    mMediaPlayer.reset();
                    mMediaPlayer.setDataSource(path);
                    mMediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            play();
        }

        public void open(List list, int position) throws RemoteException {
            if (mSongList == null || !mSongList.containsAll(list))
                mSongList = list;
            mPlayPosition = position;
            play();
        }

        public int getQueuePosition() {
            return mPlayPosition;
        }

        public boolean isPlaying() {
            return mIsSupposedToBePlaying;
        }

        public void stop() {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
            }
            mIsSupposedToBePlaying = false;
        }

        public void pause() {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
            mIsSupposedToBePlaying = false;
        }

        public void prev() throws RemoteException {
            if (isPlaying()) {
                if (mShuffleMode == 0) {
                    if (mPlayPosition == 0) {
                        mPlayPosition = mSongList.size() - 1;
                    } else {
                        mPlayPosition -= 1;
                    }
                } else {
                    mPlayPosition = new Random().nextInt(mSongList.size());
                }
                play();
            }
        }

        public void next() throws RemoteException {
            if (isPlaying()) {
                if (mShuffleMode == 0) {
                    if (mPlayPosition == mSongList.size() - 1) {
                        mPlayPosition = 0;
                    } else {
                        mPlayPosition += 1;
                    }
                } else {
                    mPlayPosition = new Random().nextInt(mSongList.size());
                }
                play();
            }
        }

        public int duration() {
            return mSongList.get(mPlayPosition).getDuration();
        }

        public int position() {
            return 0;
        }

        public void seek(int pos) {
            mMediaPlayer.seekTo(pos);
        }

        public String getTitle() {
            return mSongList.get(mPlayPosition).getTitle();
        }

        public String getAlbum() {
            return mSongList.get(mPlayPosition).getAlbum();
        }

        public String getArtist() {
            return mSongList.get(mPlayPosition).getArtist();
        }

        public void setQueuePosition(int index) {
            if (mPlayPosition != index) {
                mPlayPosition = index;
                notifyObserver();
            }
        }

        public String getPath() {
            return mSongList.get(mPlayPosition).getPath();
        }

        public Bitmap getBitmap() {
            return mSongList.get(mPlayPosition).getAlbumBitmap();
        }

        public void setShuffleMode(int shuffleMode) {
            synchronized (this) {
                if (mShuffleMode == shuffleMode && mSongList.size() > 0) {
                    return;
                }
                mShuffleMode = shuffleMode;
//                if (mShuffleMode == 1) {
//                    if (makeAutoShuffleList()) {
//                        mPlayListLen = 0;
//                        doAutoShuffleUpdate();
//                        mPlayPosition = 0;
//                        play();
//                    } else {
//                        // failed to build a list of files to shuffle
//                        mShuffleMode = SHUFFLE_NONE;
//                    }
//                }
            }
        }

        public int getShuffleMode() {
            return mShuffleMode;
        }


        public void setRepeatMode(int repeatMode) {

        }

        public int getRepeatMode() {
            return mRepeatMode;
        }
    }


    public interface Observer {
        void update(List<SongViewModel> list);
    }

    private void notifyObserver() {
        if (mObserver != null) {
            synchronized (mSongList) {
                this.mObserver.update(mSongList);
            }
        }
    }

    /**
     * Returns whether something is currently playing
     *
     * @return true if something is playing (or will be playing shortly, in case
     * we're currently transitioning between tracks), false if not.
     */
    public boolean isPlaying() {
        return mIsSupposedToBePlaying;
    }

    private void updateNotification() throws RemoteException {
        RemoteViews subNotificationLayout = new RemoteViews(getPackageName(), R.layout.statusbar_appwidget_s);
        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.statusbar_appwidget_l);

        Intent notificationIntent = new Intent(this, ActivityMusic.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Intent previousIntent = new Intent(this, MediaPlaybackService.class);
        previousIntent.setAction("Previous");
        PendingIntent previousPendingIntent = null;

        Intent playIntent = new Intent(this, MediaPlaybackService.class);
        playIntent.setAction("Play");
        PendingIntent playPendingIntent = null;

        Intent nextIntent = new Intent(this, MediaPlaybackService.class);
        nextIntent.setAction("Next");
        PendingIntent nextPendingIntent = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            previousPendingIntent = PendingIntent.getForegroundService(this, 0, previousIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            playPendingIntent = PendingIntent.getForegroundService(this, 0, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            nextPendingIntent = PendingIntent.getForegroundService(this, 0, nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        Bitmap bitmap = ServiceStub.asInterface(mBinder).getBitmap();

        notificationLayout.setOnClickPendingIntent(R.id.notify_prev, previousPendingIntent);
        notificationLayout.setOnClickPendingIntent(R.id.notify_play, playPendingIntent);
        notificationLayout.setOnClickPendingIntent(R.id.notify_next, nextPendingIntent);
        notificationLayout.setImageViewBitmap(R.id.notify_bitmap, bitmap == null ? BitmapFactory.decodeResource(getResources(), R.drawable.album_cover_background) : bitmap);
        notificationLayout.setTextViewText(R.id.notify_title, ServiceStub.asInterface(mBinder).getTitle());
        notificationLayout.setTextViewText(R.id.notify_artist, ServiceStub.asInterface(mBinder).getArtist());
        notificationLayout.setTextViewText(R.id.notify_album, ServiceStub.asInterface(mBinder).getAlbum());
        notificationLayout.setImageViewResource(R.id.notify_play, isPlaying() ? R.drawable.pause_outline : R.drawable.play_outline);

        subNotificationLayout.setOnClickPendingIntent(R.id.notify_prev, previousPendingIntent);
        subNotificationLayout.setOnClickPendingIntent(R.id.notify_play, playPendingIntent);
        subNotificationLayout.setOnClickPendingIntent(R.id.notify_next, nextPendingIntent);
        subNotificationLayout.setImageViewBitmap(R.id.notify_bitmap, bitmap == null ? BitmapFactory.decodeResource(getResources(), R.drawable.album_cover_background) : bitmap);
        subNotificationLayout.setImageViewResource(R.id.notify_play, isPlaying() ? R.drawable.pause_outline : R.drawable.play_outline);

        Notification notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ONE_ID)
                .setSmallIcon(R.drawable.album_cover_background)
                .setPriority(2)
                .setCustomContentView(subNotificationLayout)
                .setCustomBigContentView(notificationLayout)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
    }
}