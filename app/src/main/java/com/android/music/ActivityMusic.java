package com.android.music;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.music.fragments.BaseSongListFragment;

import java.io.FileNotFoundException;
import java.util.List;

public class ActivityMusic extends AppCompatActivity implements BaseSongListFragment.BaseSongListFragmentClickListener {

    private static final String TAG = "ActivityMusic";
    private Context mContext = ActivityMusic.this;
    private boolean mServiceBound;
    public IMediaPlaybackService mService;
    private List<SongViewModel> songViewModelList;

    private MusicUtils.ServiceToken mToken;
    private BaseSongListFragment baseSongListFragment;
    private Fragment mMediaPlaybackFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_music);
        //Checks permissions on Android 6 and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
        // Bind to service
        Intent intent = new Intent(getApplicationContext(), MediaPlaybackService.class);
        bindService(intent, mConnection, BIND_AUTO_CREATE);
        try {
            songViewModelList = SongRepository.getSongList(this);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        baseSongListFragment = new BaseSongListFragment(songViewModelList);
        initUI();
    }
    @Override
    protected void onStart() {
        super.onStart();

    }

    private void initUI() {
        // Get the FragmentManager.
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.fragment_container, baseSongListFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        // Called when the connection with the service is established
        public void onServiceConnected(ComponentName className, IBinder service) {
            // Following the example above for an AIDL interface,
            // this gets an instance of the IRemoteInterface, which we can use to call on the service
            mService = IMediaPlaybackService.Stub.asInterface(service);
        }

        // Called when the connection with the service disconnects unexpectedly
        public void onServiceDisconnected(ComponentName className) {
//            Log.e(TAG, "Service has unexpectedly disconnected");
            mService = null;
        }
    };

    //Handling callback
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mConnection);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBaseSongListFragmentClick(int position) {
        try {
            mService.open(songViewModelList, position);
            baseSongListFragment.updateHeader();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void onHeaderClick(View view) {
        getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.all_songs_fragment, mMediaPlaybackFragment).commit();
        findViewById(R.id.header_layout).setVisibility(View.GONE);
    }
}