// IMediaPlaybackService.aidl
package com.android.music;

// Declare any non-default types here with import statements
import android.graphics.Bitmap;

interface IMediaPlaybackService {

    boolean isPlaying();
    void openFile(String path);
    void open(in List list, int position);
    void play();
    void stop();
    void pause();
    void prev();
    void next();
    void seek(int pos);
    int duration();
    int position();
    String getTitle();
    String getAlbum();
    String getArtist();
    String getPath();
    Bitmap getBitmap();
    int getQueuePosition();
    void setQueuePosition(int index);
    int getShuffleMode();
    void setShuffleMode(int shufflemode);
    int getRepeatMode();
    void setRepeatMode(int repeatmode);
}